package com.gitlab.firelight.currencymod;

import com.gitlab.firelight.currencymod.item.Groups;
import com.gitlab.firelight.currencymod.item.ModItems;
import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CurrencyMod implements ModInitializer {
    public static final String MOD_ID = "currencymod";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

    @Override
    public void onInitialize() {
        Groups.registerItemGroups();
        ModItems.registerModItems();
    }
}
